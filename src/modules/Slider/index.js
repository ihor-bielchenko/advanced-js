import Slider from './Slider.jsx';
import handleChangeSlide from './handleChangeSlide.js';

export default Slider;
export {
	handleChangeSlide,
};
